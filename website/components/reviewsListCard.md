# reviewsListCard

## Props

<!-- @vuese:reviewsListCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|header|-|`String`|`false`|-|
|content|-|`Array`|`false`|-|
|limit|-|`Number`|`false`|-|
|page|-|`Number`|`false`|-|
|update|-|`Array`|`false`|-|
|own|-|`String`|`false`|-|

<!-- @vuese:reviewsListCard:props:end -->


## Events

<!-- @vuese:reviewsListCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|nextPage|-|-|
|previousPage|-|-|
|deleteUpdate|-|-|

<!-- @vuese:reviewsListCard:events:end -->



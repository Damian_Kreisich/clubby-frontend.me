# customNav

## Props

<!-- @vuese:customNav:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|listType|-|`Number`|`false`|-|
|photo|-|`String`|`false`|-|

<!-- @vuese:customNav:props:end -->



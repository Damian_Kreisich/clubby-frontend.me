# rating

## Props

<!-- @vuese:rating:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|rating|-|`Number`|`false`|-|
|header|-|`String`|`false`|-|

<!-- @vuese:rating:props:end -->



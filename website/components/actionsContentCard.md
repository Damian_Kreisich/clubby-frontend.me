# actionsContentCard

## Props

<!-- @vuese:actionsContentCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|header|-|`String`|`false`|-|
|content|-|`Array`|`false`|-|
|maximize|-|`String`|`false`|-|
|limit|-|`Number`|`false`|-|
|page|-|`Number`|`false`|-|

<!-- @vuese:actionsContentCard:props:end -->


## Events

<!-- @vuese:actionsContentCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|nextPage|-|-|
|previousPage|-|-|

<!-- @vuese:actionsContentCard:events:end -->



# ownReviewListCard

## Props

<!-- @vuese:ownReviewListCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|header|-|`String`|`false`|-|
|content|-|`Array`|`false`|-|
|limit|-|`Number`|`false`|-|
|page|-|`Number`|`false`|-|
|entity|-|`String`|`false`|-|

<!-- @vuese:ownReviewListCard:props:end -->


## Events

<!-- @vuese:ownReviewListCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|nextPage|-|-|
|previousPage|-|-|
|delete|-|-|

<!-- @vuese:ownReviewListCard:events:end -->



# reviewFormCard

## Props

<!-- @vuese:reviewFormCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|entity|-|`String`|`false`|-|
|update|-|`Object`|`false`|-|

<!-- @vuese:reviewFormCard:props:end -->


## Events

<!-- @vuese:reviewFormCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|postData|-|-|

<!-- @vuese:reviewFormCard:events:end -->



# searchCard

## Props

<!-- @vuese:searchCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|topText|-|`String`|`false`|-|
|labelText|-|`String`|`false`|-|
|helperText|-|`String`|`false`|-|

<!-- @vuese:searchCard:props:end -->



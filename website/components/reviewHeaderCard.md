# reviewHeaderCard

## Props

<!-- @vuese:reviewHeaderCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|header|-|`String`|`false`|-|
|content|-|`Array`|`false`|-|
|entity|-|`String`|`false`|-|

<!-- @vuese:reviewHeaderCard:props:end -->



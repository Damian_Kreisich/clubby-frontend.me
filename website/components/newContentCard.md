# newContentCard

## Props

<!-- @vuese:newContentCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|header|-|`String`|`false`|-|
|content|-|`Array`|`false`|-|
|maximize|-|`String`|`false`|-|
|limit|-|`Number`|`false`|-|
|page|-|`Number`|`false`|-|
|entity|-|`String`|`false`|-|
|own|-|`String`|`false`|-|

<!-- @vuese:newContentCard:props:end -->


## Events

<!-- @vuese:newContentCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|nextPage|-|-|
|previousPage|-|-|
|delete|-|-|

<!-- @vuese:newContentCard:events:end -->



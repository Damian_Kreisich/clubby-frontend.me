# contactForm

## Props

<!-- @vuese:contactForm:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|headlineTop|-|`String`|`false`|-|
|headlineAdditional|-|`String`|`false`|-|
|helperTextMessage|-|`String`|`false`|-|
|defaultName|-|`String`|`false`|-|
|defaultMail|-|`String`|`false`|-|
|recipientMail|-|`String`|`false`|-|

<!-- @vuese:contactForm:props:end -->



# basicInfoCard

## Props

<!-- @vuese:basicInfoCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|informations|-|`Array`|`false`|-|
|header|-|`String`|`false`|-|

<!-- @vuese:basicInfoCard:props:end -->



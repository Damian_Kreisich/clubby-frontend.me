# Advanced Search Card

Advanced search card is component defined in advancedSearchCard.vue  
Destination of the component is to provide card constrained to material design card component. It is capable of providing search view to embed into another views.
## Props

<!-- @vuese:advancedSearchCard:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|topText|Defines card heading text to be displayed|`String`|`true`|-|
|labelText|Defines label that is describing input field|`String`|`true`|-|
|helperText|Defines textual tip displayed under input field|`String`|`true`|-|
|mainInput|Defines text that apears as a placeholder in main input field|`String`|`true`|-|
|entity|Defines which entity should be subject of search|`String`|`true`|-|

<!-- @vuese:advancedSearchCard:props:end -->
## Events

<!-- @vuese:advancedSearchCard:events:start -->
|Event Name|Description|Parameters|
|---|---|---|
|filters|Shows if filters are being used inside the component |-|
|queryChanged|Shows if there is a need to collect data for search call|searchData|

<!-- @vuese:advancedSearchCard:events:end -->



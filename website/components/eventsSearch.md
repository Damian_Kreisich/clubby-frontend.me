# eventsSearch

## Props

<!-- @vuese:eventsSearch:props:start -->
|Name|Description|Type|Required|Default|
|---|---|---|---|---|
|initialSearchTerm|-|`String`|`false`|-|

<!-- @vuese:eventsSearch:props:end -->



import apiCall from "../../services/apiMock";
import axios from "axios";
import {
  AUTH_ERROR,
  AUTH_REQUEST,
  AUTH_LOGOUT,
  AUTH_SUCCESS,
  USER_REQUEST,
  AUTH_ERROR_TOGGLE
} from "../storeConstants";
import { Store } from "vuex";

const state = { token: "" || "", status: "", hasLoadedOnce: false };

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
};

const actions = {
  [AUTH_REQUEST]: ({ commit, dispatch }, user) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST);
      axios({
        url: "/api/auth/login",
        data: user,
        method: "post"
      })
        .then(resp => {
          axios.defaults.headers.common["Authorization"] =
            "Bearer " + resp.data.access_token;
          commit(AUTH_SUCCESS, resp);
          dispatch(USER_REQUEST);
          resolve(resp);
        })
        .catch(err => {
          commit(AUTH_ERROR, err);
          reject();
        });
    });
  },
  [AUTH_LOGOUT]: ({ commit, dispatch }) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_LOGOUT);
      resolve();
    });
  },
  [AUTH_ERROR_TOGGLE]: ({ commit, dispatch }, state) => {
    return new Promise((resolve, reject) => {
      if (state.auth.status == "error") {
        commit(AUTH_ERROR_TOGGLE);
        resolve();
      } else {
        reject();
      }
    });
  }
};

const mutations = {
  [AUTH_REQUEST]: state => {
    state.status = "loading";
  },
  [AUTH_SUCCESS]: (state, resp) => {
    state.status = "success";
    state.token = "Bearer " + resp.data.access_token;
    state.hasLoadedOnce = true;
  },
  [AUTH_ERROR]: state => {
    state.status = "error";
    state.hasLoadedOnce = true;
  },
  [AUTH_LOGOUT]: state => {
    state.token = "";
    state.hasLoadedOnce = false;
  },
  [AUTH_ERROR_TOGGLE]: state => {
    if (state.status == "error") state.status = "";
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};

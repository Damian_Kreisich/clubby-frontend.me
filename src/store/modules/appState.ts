import {
  NOT_LOGGED,
  HOME_VIEW,
  CLUB_DETAIL,
  EVENT_DETAIL,
  MENU_START,
  MENU_TOGGLE,
  LAST_PAGE,
  MENU_LOGO,
  MENU_START_LOGO,
  CLUB_PREVIEW,
  EVENT_PREVIEW
} from "../storeConstants";
import Vue from "vue";

const state = {
  appState: -1,
  menuVisible: true,
  bigScreen: true,
  lastPath: "/",
  logo: require("../../assets/logo.png")
};

const getters = {
  getAppState: state => state.appState,
  isMenuVisible: state => state.menuVisible,
  isBigScreen: state => state.bigScreen
};

const mutations = {
  [NOT_LOGGED]: state => {
    state.appState = -1;
    state.logo = require("../../assets/logo.png");
  },
  [HOME_VIEW]: state => {
    state.appState = 0;
    state.logo = require("../../assets/logo.png");
  },
  [CLUB_DETAIL]: (state, logo) => {
    state.appState = 1;
    state.logo = logo;
  },
  [EVENT_DETAIL]: (state, logo) => {
    state.appState = 2;
    state.logo = logo;
  },
  [MENU_START]: state => {
    state.menuVisible = screen.width > 768;
    state.bigScreen = screen.width > 768;
  },
  [MENU_TOGGLE]: state => {
    state.menuVisible = !state.menuVisible;
    state.bigScreen = screen.width > 768;
  },
  [LAST_PAGE]: (state, fromPath) => {
    state.lastPath = fromPath;
  },
  [MENU_LOGO]: (state, logo) => {
    state.logo = logo;
  },
  [MENU_START_LOGO]: state => {
    state.logo = require("../../assets/logo.png");
  },
  [CLUB_PREVIEW]: (state, logo) => {
    state.appState = 3;
    state.logo = logo;
  },
  [EVENT_PREVIEW]: (state, logo) => {
    state.appState = 4;
    state.logo = logo;
  }
};

const actions = {
  [MENU_TOGGLE]: ({ state, commit, dispatch }) => {
    return new Promise((resolve, reject) => {
      if (state.menuVisible && !state.bigScreen) {
        commit(MENU_TOGGLE);
      }
      resolve();
    });
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};

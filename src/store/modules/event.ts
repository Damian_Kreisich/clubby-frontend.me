import { LOAD_EVENT, EVENT_DETAIL } from "../storeConstants";
import Vue from "vue";
import axios from 'axios';

const state = {
  event: {}
};

const getters = {
  getEvent: state => () => state.event
};

const mutations = {
  [LOAD_EVENT]: (state, event) => {
    state.event = event;
  }
};

const actions = {
  [LOAD_EVENT]: ( {commit, dispatch}, id ) => {
    axios({
      method: "get",
      url: `/api/event/${id}`
    }).then(response => {
      commit(LOAD_EVENT, response.data.data);
      commit(EVENT_DETAIL, response.data.data.Logo);
    }).catch(error => {
      Vue.$log.error(error);
    });
    }
  };

export default {
  state,
  getters,
  actions,
  mutations
};

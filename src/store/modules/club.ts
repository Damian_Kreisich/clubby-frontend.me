import { LOAD_CLUB, CLUB_DETAIL } from "../storeConstants";
import Vue from "vue";
import axios from 'axios';

const state = {
  club: {}
};

const getters = {
  getClub: state => state.club
};

const mutations = {
  [LOAD_CLUB]: (state, club) => {
    state.club = club;
  }
};

const actions = {
  [LOAD_CLUB]: ( {commit, dispatch}, id ) => {
    axios({
      method: "get",
      url: `/api/club/${id}`
    }).then(response => {
      commit(LOAD_CLUB, response.data.data);
      commit(CLUB_DETAIL, response.data.data.Logo);
    }).catch(error => {
      Vue.$log.error(error);
    });
    }
  };

export default {
  state,
  getters,
  actions,
  mutations
};

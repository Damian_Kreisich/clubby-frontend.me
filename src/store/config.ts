import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'
import auth from './modules/auth'
import appState from './modules/appState'
import createPersistedState from 'vuex-persistedstate'
import club from './modules/club'
import event from './modules/event'
import SecureLS from 'secure-ls'
var ls = new SecureLS({ isCompression: false });

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const plugins = [
  createPersistedState({
    paths: ["user", "auth", "appState"],
    storage: {
      getItem: key => ls.get(key),
      setItem: (key, value) => ls.set(key, value),
      removeItem: key => ls.remove(key)
    }
  })
]

export default new Vuex.Store({
  modules: {
    user,
    auth,
    appState,
    club,
    event
  },
  plugins: plugins,
  strict: debug,
})
export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
export const AUTH_ERROR_TOGGLE = "AUTH_ERROR_TOGGLE";

export const USER_REQUEST = "USER_REQUEST";
export const USER_SUCCESS = "USER_SUCCESS";
export const USER_ERROR = "USER_ERROR";

export const NOT_LOGGED = "NOT_LOGGED";
export const HOME_VIEW = "HOME_VIEW";
export const CLUB_DETAIL = "CLUB_DETAIL";
export const EVENT_DETAIL = "EVENT_DETAIL";

export const MENU_START = "MENU_START";
export const MENU_TOGGLE = "MENU_TOGGLE";
export const MENU_LOGO = "MENU_LOGO";
export const MENU_START_LOGO = "MENU_START_LOGO";

export const LAST_PAGE = "LAST_PAGE";
export const LOAD_CLUB = "LOAD_CLUB";
export const LOAD_EVENT = "LOAD_EVENT";

export const CLUB_PREVIEW = "CLUB_PREVIEW";
export const EVENT_PREVIEW = "EVENT_PREVIEW";
export default class Genre {
    ID !: number
    name !: string
    public static createGenre(id: number, name: string): Genre{
        const genre = new Genre();
        genre.ID = id;
        genre.name = name;
        return genre;
    }
}
import City from './City';
import { Dictionary } from 'vue-router/types/router';
import TimeFrame from './TimeFrame';

export default class Club{
    ID !: number;
    name !: string;
    city !: City;
    overallRating !: number;
    logo !:string;
    music !: string;
    shortDescription!: string;
    fbContentURL!: string;
    aboutUsContent!: string;
    openTime!: Dictionary<TimeFrame>;
    address!: string;
    clubType!: string;

    public static createClub(id: number, name: string, city: City, overallRating: number, logo: string, music:string, shortDescription: string) : Club {
        const inst = new Club();

        inst.ID = id;
        inst.name = name;
        inst.city = city;
        inst.overallRating = overallRating;
        inst.logo = logo;
        inst.music = music;
        inst.shortDescription = shortDescription;

        return inst;
    }
    public static createFullClub(id: number, name: string, city: City, overallRating: number, logo: string, music:string, 
        shortDescription: string, openTime: Dictionary<TimeFrame>, address: string, clubType: string) : Club {
        const inst = new Club();

        inst.ID = id;
        inst.name = name;
        inst.city = city;
        inst.overallRating = overallRating;
        inst.logo = logo;
        inst.music = music;
        inst.shortDescription = shortDescription;

        return inst;
    }
}
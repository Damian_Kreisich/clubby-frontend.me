export default class City{
    ID !: number
    name !: string
    public static createCity(id: number, name: string): City{
        const city = new City();
        city.ID = id;
        city.name = name;
        return city;
    }

    public toString(){
        return this.name;
    }
}
export default class Review {
  ID!: number;
  clubID!: number;
  eventID!: number;
  username!: string;
  factor1!: number;
  factor2!: number;
  factor3!: number;
  factor4!: number;
  overall!: number;
  content!: string;
  
  public static createReview(resp): Review {
    const review = new Review();
    review.ID = resp.ID;
    review.clubID = resp.clubID;
    review.eventID = resp.eventID;
    review.username = resp.username;
    review.factor1 = resp.factor1;
    review.factor2 = resp.factor2;
    review.factor3 = resp.factor3;
    review.factor4 = resp.factor4;
    review.overall = resp.overall;
    review.content = resp.content;

    return review;
  }
}

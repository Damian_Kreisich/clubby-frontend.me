import BasicUserDTO from './DTO/BasicUserDTO';
import DetailedUserDTO from './DTO/DetailedUserDTO';
import City from './City';

export default class AppUser{
    ID!: number;
    displayName!: string;
    personalDesc!: string;
    dateOfBirth!: Date;
    mainCity!: City;
    photoSmall!: string;
    photoBig!: string;

    public static basicUser(dto : BasicUserDTO) : AppUser{
        const user = new AppUser();

        user.ID = dto.ID;
        user.displayName = dto.displayName;

        return user
    }

    public static wholeUser(dto: DetailedUserDTO) : AppUser{
        const user = new AppUser();
        
        user.ID = dto.ID;
        user.dateOfBirth = dto.dateOfBirth;
        user.displayName = dto.displayName;
        user.mainCity = dto.mainCity;
        user.personalDesc = dto.personalDesc;
        user.photoBig = dto.photoBig;
        user.photoSmall = dto.photoSmall;

        return user;
    }
}
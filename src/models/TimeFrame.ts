
export default class TimeFrame {
    open!: string;
    close!: string;

    public static createTimeFrame(open: string, close: string) : TimeFrame {
        const timeframe = new TimeFrame();
        timeframe.open = open;
        timeframe.close = close;

        return timeframe;
    }
}
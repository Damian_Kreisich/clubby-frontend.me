import City from '../City';
import BasicUserDTO from './BasicUserDTO';

export default class DetailedUserDTO extends BasicUserDTO{
    personalDesc!: string;
    dateOfBirth!: Date;
    mainCity!: City;
    photoSmall!: string;
    photoBig!: string;
}
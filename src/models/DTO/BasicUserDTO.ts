export default class BasicUserDTO{
    ID!: number;
    displayName!: string;
    constructor(ID : number, displayName: string) {
        this.ID = ID;
        this.displayName = displayName;
    }
}
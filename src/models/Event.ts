
import { Dictionary } from 'vue-router/types/router';
import TimeFrame from './TimeFrame';
import Club from './Club';
import Genre from './Genre';

export default class Event{
    ID !: number;
    siteStatsID !: number;
    name !: string;
    club !: Club;
    date !: string;
    timeStart !: string;
    timeEnd !: string;
    logo !:string;
    genres !: Genre[];
    bands !: string[];
    ticketSites !: string[];
    shortDescription!: string;
    longDescription!: string;
    fbContentURL!: string;
    soldOut!: boolean;
    Attendees!: number;

    public static createEvent(id: number, name: string, club: Club, date: string, logo: string, genres: Genre[], shortDescription: string) : Event {
        const inst = new Event();

        inst.ID = id;
        inst.name = name;
        inst.club = club;
        inst.date = date;
        inst.logo = logo;
        inst.genres = genres;
        inst.shortDescription = shortDescription;

        return inst;
    }
    public static createFullEvent(id: number, name: string, club: Club, date: string, logo: string, genres: Genre[], shortDescription: string
        , timeStart: string, timeEnd: string, fbContentURL: string, longDescription: string, bands: string[], soldOut: boolean) : Event {
        const inst = new Event();

        inst.ID = id;
        inst.name = name;
        inst.club = club;
        inst.date = date;
        inst.logo = logo;
        inst.genres = genres;
        inst.shortDescription = shortDescription;
        inst.bands = bands;
        inst.date = date;
        inst.fbContentURL = fbContentURL;
        inst.timeStart = timeStart;
        inst.timeEnd = timeEnd;
        inst.longDescription = longDescription;
        inst.soldOut = soldOut;

        return inst;
    }
}
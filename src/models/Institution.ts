import City from './City';

export default class Institution {
    ID !: number;
    name !: string;
    city !: City;
    overallRating !: number;
    logo !:string;
    music !: string;
    shortDescription!: string;

    public static createInstitution(id: number, name: string, city: City, overallRating: number, logo: string, music:string, shortDescription: string) : Institution {
        const inst = new Institution();

        inst.ID = id;
        inst.name = name;
        inst.city = city;
        inst.overallRating = overallRating;
        inst.logo = logo;
        inst.music = music;
        inst.shortDescription = shortDescription;

        return inst;
    }
}
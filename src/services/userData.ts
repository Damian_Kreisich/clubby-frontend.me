import AppUser from "../models/AppUser";
import BasicUserDTO from '@/models/DTO/BasicUserDTO';

export default class UserDataBinder{
    user !: AppUser

    //TODO
    public static createSimpleBinder(displayName: string){
        const binder = new UserDataBinder();
        binder.user = AppUser.basicUser(new BasicUserDTO(0, displayName));
        return binder;
    }

    public sendTopbarData(){
        return this.user.displayName;
    }
}
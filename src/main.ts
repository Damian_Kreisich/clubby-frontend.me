import Vue from 'vue'
import VueMaterial from 'vue-material'
import VueResource from 'vue-resource'
import VueLogger from 'vuejs-logger'
import axios from 'axios'

import 'vue-material/dist/vue-material.min.css'
import './app-theme.scss'

import App from './App.vue'

import router from './router/config'
import store from './store/config'

Vue.use(VueResource)
Vue.use(VueMaterial)

axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Accept'] = 'application/json'

Vue.config.productionTip = false

declare module 'vue/types/vue' {
  export interface VueConstructor<V extends Vue = Vue> {
    $log: {
      debug(...args: any[]): void;
      info(...args: any[]): void;
      warn(...args: any[]): void;
      error(...args: any[]): void;
      fatal(...args: any[]): void;
    };
  }
}

const options = {
  isEnabled: true,
  logLevel : 'debug',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
};

Vue.use(VueLogger, options);

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')

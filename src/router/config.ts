import Vue from "vue";
import Router from "vue-router";
import Register from "../components/register.vue";
import Login from "../components/login.vue";
import Home from "../components/home.vue";
import Help from "../components/help.vue";
import store from "../store/config";
import Clubs from "../components/clubs.vue";
import ClubsSearch from "../components/clubsSearch.vue";
import ClubList from "../components/clubsList.vue";
import ClubDetails from "../components/clubDetails.vue";
import ClubContact from "../components/contactClub.vue";
import Events from "../components/events.vue";
import ContactEvents from "../components/contactEvent.vue";
import EventDetails from "../components/eventDetails.vue";
import EventSearch from "../components/eventsSearch.vue";
import EventsList from "../components/eventsList.vue";
import NewsPage from "../components/newsPage.vue";
import Settings from "../components/settings.vue";
import ClubInfo from "../components/clubInformations.vue";
import ClubRatings from "../components/clubRatings.vue";
import ClubReviewDetails from "../components/clubReviewDetails.vue";
import CreateReviewForm from "../components/createReviewForm.vue";
import OwnClubsList from "../components/ownClubsList.vue";
import OwnEventsList from "../components/ownEventsList.vue";
import ClubEvents from "../components/clubEvents.vue";
import ClubAbout from "../components/clubAbout.vue";
import Ratings from "../components/ratings.vue";
import EventRatings from "../components/eventRatings.vue";
import EventReviewDetails from "../components/eventReviewDetails.vue";
import EventInfo from "../components/eventInformations.vue";
import UpdateEvent from "../components/updateEventForm.vue";
import UpdateClub from "../components/updateClubForm.vue";
import axios from "axios";
import {
  NOT_LOGGED,
  HOME_VIEW,
  MENU_START_LOGO,
  LAST_PAGE,
  MENU_TOGGLE,
  LOAD_CLUB,
  LOAD_EVENT,
  EVENT_PREVIEW,
  CLUB_PREVIEW
} from "../store/storeConstants";

Vue.use(Router);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    store.commit(NOT_LOGGED);
    store.commit(MENU_START_LOGO);
    next();
    return;
  }
  next("/home");
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    store.commit(HOME_VIEW);
    store.commit(MENU_START_LOGO);
    next();
    return;
  }
  next("/login");
};

const ifAuthenticatedClub = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    if (to.params.id == "preview"){
      store.commit(CLUB_PREVIEW, store.getters.getClub.logo);
      next();
    }
    else if (store.getters.getClub.ID !== to.params.id){
      store.dispatch(LOAD_CLUB, to.params.id);
    }
    next();
    return;
  }
  next("/login");
};

const ifAuthenticatedEvent = (to, from, next) => { 
  if (store.getters.isAuthenticated) {
    if (to.params.id == "preview"){
      store.commit(EVENT_PREVIEW, store.getters.getEvent().logo);
      next();
    }
    else if (store.getters.getEvent().ID !== to.params.id){
      store.dispatch(LOAD_EVENT, to.params.id);
    }
    next();
    return;
  }
  next("/login");
};

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/home",
      name: "Home",
      component: Home,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      beforeEnter: ifNotAuthenticated
    },
    {
      path: "/clubs",
      name: "Clubs",
      component: Clubs,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/ratings",
      name: "User ratings",
      component: Ratings,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/clubs/search",
      name: "Clubs search",
      component: ClubsSearch,
      props: true,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/clubs/:cityID",
      name: "Clubs list",
      component: ClubList,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/clubs/details/:id",
      name: "Club details",
      component: ClubDetails,
      beforeEnter: ifAuthenticatedClub,
      redirect: "/clubs/details/:id/news"
    },
    {
      path: "/clubs/details/:id/contact",
      name: "Club contact",
      component: ClubContact,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: "/clubs/details/:id/about",
      name: "Club about",
      component: ClubAbout,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: "/clubs/details/:id/news",
      name: "Club news",
      component: NewsPage,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: "/clubs/details/:id/info",
      name: "Club info",
      component: ClubInfo,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: '/clubs/details/:id/ratings',
      name: "Club ratings",
      component: ClubRatings,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: '/clubs/update/:id',
      name: "Club update",
      component: UpdateClub,
      beforeEnter: ifAuthenticated 
    },
    {
      path: '/events/update/:id',
      name: "Event update",
      component: UpdateEvent,
      beforeEnter: ifAuthenticated
    },
    {
      path: '/clubs/details/:id/events',
      name: "Club events",
      component: ClubEvents,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: "/reviews/club/:id/create",
      name: "Create review for club",
      component: CreateReviewForm,
      beforeEnter: ifAuthenticatedClub
    },
    {
      path: "/reviews/event/:id/create",
      name: "Create review for event",
      component: CreateReviewForm,
      beforeEnter: ifAuthenticatedEvent
    },
    {
      path: "/help",
      name: "Help",
      component: Help
    },
    {
      path: "/events",
      name: "Events",
      component: Events,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/events/search",
      name: "Events search",
      component: EventSearch,
      props: true,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/events/:cityID",
      name: "Events list",
      component: EventsList,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/events/details/:id",
      name: "Events details",
      component: EventDetails,
      beforeEnter: ifAuthenticatedEvent,
      redirect: "/events/details/:id/info"
    },
    {
      path: "/events/details/:id/contact",
      name: "Events contact",
      component: ContactEvents,
      beforeEnter: ifAuthenticatedEvent
    },
    {
      path: "/events/details/:id/info",
      name: "Event info",
      component: EventInfo,
      beforeEnter: ifAuthenticatedEvent
    },
    {
      path: "/events/details/:id/ratings",
      name: "Event ratings",
      component: EventRatings,
      beforeEnter: ifAuthenticatedEvent
    },
    {
      path: "/settings",
      name: "Settings",
      component: Settings,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/club/own",
      name: "Own clubs",
      component: OwnClubsList,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/event/own",
      name: "Own events",
      component: OwnEventsList,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/reviews/club/details/:id",
      name: "Reviews details club",
      component: ClubReviewDetails,
      beforeEnter: ifAuthenticated
    },
    {
      path: "/reviews/events/details/:id",
      name: "Reviews details event",
      component: EventReviewDetails,
      beforeEnter: ifAuthenticated
    },
    {
      path: "*",
      name: "NotFound",
      redirect: "/home"
    }
  ]
});

router.beforeEach((to, from, next) => {
  axios.defaults.headers.common['Authorization'] = (store.state as any).auth.token;
  store.commit(LAST_PAGE, from.path);
  store.dispatch(MENU_TOGGLE);
  next();
});

export default router;
